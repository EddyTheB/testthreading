# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 10:38:36 2018

This is a script I have written to try to understand the threading
module, and to test some aspects of it that I think will be useful.

We call, as a thread, a simple function that does not much
other than to write to a log file (and stdout) every X seconds.

Meanwhile, every Y seconds (where Y-1 > X) we check the thread to see if it is
a. still_alive, and b. not hanging. We assume that a thread is hanging if the
log file has not been updated in Y-1 seconds.

If test a fails, i.e. an exception was raised in the thread, then we simply
allow the whole process to die.

If test b fails, i.e. the log file has not been updated in the last Y-1
seconds, then we kill the thread and restart it.

A rudimentary kill switch for the thread is also created, using a global
variable 'kill' that is triggered by the standard keyboard interrupt.

@author: edward.barratt
"""

import os
import sys
import logging
import threading
from datetime import datetime
import time

class testThread(threading.Thread):
  def __init__(self, ti=0, m=-1, s=2, e=-1, p=[-1, 0]):
    """
    The threading class designed for the function testFun.
    """
    threading.Thread.__init__(self)
    self.ti = ti
    self.m = m
    self.s = s
    self.e = e
    self.pi = p[0]
    self.ps = p[1]

  def run(self):
    try:
      testFun(ti=self.ti, m=self.m, s=self.s, e=self.e, p=[self.pi, self.ps])
    except Exception:
      sys.excepthook(*sys.exc_info())


def logging_exception_handler(type, value, tb):
  logger.exception("Uncaught exception: {0}".format(str(value)))

def testFun(ti=0, m=-1, s=2, e=-1, p=[-1, 0]):
  """
  The test function itself. Not the call to global variable 'kill'.
  """
  loggerM = logger.getChild('testFun')
  # print and log some info.
  loggerM.info('testFun called!')
  if m < ti:
    loggerM.info('This will iterate indefinately, once every {} seconds.'.format(s))
  else:
    loggerM.info('This will iterate from {} to {}, once every {} seconds.'.format(ti, m, s))
  if e >= ti:
    loggerM.info('An Exception will be raised at iteration {}.'.format(e))
  if p[0] >= ti:
    loggerM.info('The function will be paused for {} seconds at iteration {}.'.format(p[1], p[0]))
  # The core function loop.
  while True:
    if kill:
      raise ValueError('Thread killed using kill flag.')
    time.sleep(s)
    loggerM.info('Test function iteration {} run at {}.'.format(ti, datetime.strftime(datetime.now(), '%H:%M:%S')))
    ti += 1
    if ti == e:
      raise Exception('Exception!!!')
    if ti == m:
      break
    if ti == p[0]:
      time.sleep(p[1])

def prepareLogger(logfilename, loggerName):

  logger = logging.getLogger(loggerName)
  logger.setLevel(logging.INFO)

  fileFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  #streamFormatter = logging.Formatter('%(asctime)s - %(message)s')
  streamFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  logfilehandler = logging.FileHandler(logfilename)
  logfilehandler.setFormatter(fileFormatter)
  logstreamhandler = logging.StreamHandler()
  logstreamhandler.setFormatter(streamFormatter)
  logger.addHandler(logfilehandler)
  logger.addHandler(logstreamhandler)
  return logger

def main():
  global logger, kill

  # Ensure that exceptions are written to the log file.
  sys.excepthook = logging_exception_handler

  # Create the logger.
  logfilename = 'log.txt'
  loggerName = __name__
  logger = prepareLogger(logfilename, loggerName)

  # set the global variable 'kill'.
  kill = False

  # start the thread.
  logger.info('Creating Thread')
  thread = testThread(s=1, p=[20, 14], e=43)
  logger.info('Starting Thread')
  thread.start()
  logger.info('Thread Started')

  # Now the thread is running, we want to monitor it.
  # Every 5 seconds, check to see:
  # a. if the process is still alive, and
  # b. if the process has hung.
  checkInterval = 5
  try:
    while True:
      time.sleep(checkInterval)
      # Is it still alive?
      if not thread.is_alive():
        # It is not. An exception must have been raised within the thread,
        # break out of the while loop and therefore end the programme.
        logger.info('Thread is dead!')
        break
      else:
        # The thread is alive!
        # How old is the log file.
        lf_stats = os.stat(logfilename)
        lf_mtime = lf_stats.st_mtime
        lf_mtime = datetime.fromtimestamp(lf_mtime)
        print(lf_mtime)
        lf_age = datetime.now() - lf_mtime
        lf_age = lf_age.total_seconds()
        if lf_age > checkInterval-1:
          # The log file is older than our checkInterval.
          logger.info('Thread lives but has hung! Restarting.')
          kill = True
          thread.join() # join the thread and wait for the kill switch to be triggered.
          kill = False
          thread = testThread(s=1, e=43, p=[12, 14])
          thread.start()
        else:
          logger.info('Thread lives and is active!')
  except KeyboardInterrupt:
    # User has ctrl-c, but this info isn't passed down to the threads!
    logger.info('Keyboard Interrup triggered.')
    kill = True
    thread.join()

if __name__ == '__main__':
  main()