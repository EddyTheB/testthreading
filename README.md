# README #

This is a script I have written to try to understand the threading
module, and to test some aspects of it that I think will be useful.

We call, as a thread, a simple function that does not much
other than to write to a log file (and stdout) every X seconds.

Meanwhile, every Y seconds (where Y-1 > X) we check the thread to see if it is
a. still_alive, and b. not hanging. We assume that a thread is hanging if the
log file has not been updated in Y-1 seconds.

If test a fails, i.e. an exception was raised in the thread, then we simply
allow the whole process to die.

If test b fails, i.e. the log file has not been updated in the last Y-1
seconds, then we kill the thread and restart it.

A rudimentary kill switch for the thread is also created, using a global
variable 'kill' that is triggered by the standard keyboard interrupt.

## Usage ##
```python testThreading.py```